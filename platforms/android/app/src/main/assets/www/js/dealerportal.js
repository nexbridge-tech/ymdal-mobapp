var app = {
	// Application Constructor
    initialize: function() {
        document.addEventListener("deviceready", this.onDeviceReady.bind(this), false);

        //button for navigation back to check dr
        var exit = document.getElementById("exit");
        if(exit)
            exit.addEventListener("click", app.exitportal);

        //show contents inside accordion (ymdal dealer content)
        var accordion = document.getElementById("arrow");
        if(accordion)
            accordion.addEventListener("click", app.accordion_);

        //button for navigation to timeline page
        var timeline = document.getElementById("timelinebtn");
        if(timeline)
            timeline.addEventListener("click", app.timelinepage);

        var search = document.getElementById('search');
        if(search)
            search.addEventListener("keyup", app.search_table);
        
        //get data
        this.base_url = 'http://192.168.254.129/ymdal_api/'; //online
        //this.base_url = 'http://localhost/ymdal_api/'; //localhost
        app.get_data();
        app.load_dr_details();
        app.swipe_gesture();
    },

    //enable swipe gesture
    swipe_gesture: function() {
        $(document).on("pagecreate","#wrapper",function(){
            $(window).on("swipeleft",function(){
                //variable to get current page
                var swipe = document.title;
                var duration = 0.5; // animation time in seconds
                var direction = "left"; // animation direction - left | right | top | bottom
                if (swipe == "dealerportal") {
                    setTimeout(function() {
                        // Change your html here
                        document.location.href = "timeline.html";
                    }, 50);
                    nativetransitions.flip(duration, direction);
                    //document.location.href = "timeline.html";
                } else if (swipe == "timeline") {
                    setTimeout(function() {
                        // Change your html here
                        document.location.href = "dealerportal.html";
                    }, 50);
                    nativetransitions.flip(duration, direction);
                    //document.location.href = "dealerportal.html";
                }
            });                       
        });
    },

    //search model code
    search_table: function() {
        var field = document.getElementById('search');
        var input = field.value.toUpperCase();
        var table = document.getElementById('table-items');
        var tr = table.getElementsByTagName('tr');

        for (var i = 0; i < tr.length; i++) {
            //model code, model name, frame no, engin no, status
            var model_code = tr[i].getElementsByTagName("td")[0];
            var model_name = tr[i].getElementsByTagName("td")[1];
            var frame_no = tr[i].getElementsByTagName("td")[2];
            var engine_no = tr[i].getElementsByTagName("td")[3];
            var status = tr[i].getElementsByTagName("td")[4];

            if(model_code){
                if (model_code.innerHTML.toUpperCase().indexOf(input) > -1) {
                    tr[i].style.display = "";
                }else{
                    tr[i].style.display = "none";
                }
            }
        }
    },

    //back to check dr page
    exitportal: function() {
        var exit_portal = confirm("Exit YMDAL?");
        if(exit_portal)
            navigator.app.exitApp();
    },

    //show contents for ymdal dealer details
    accordion_: function() {
        var display = app.getDisplay('more-details');
        if (display == 'none') {
            document.getElementById('more-details').style.display = 'block';
            document.getElementById("arrow").src = "img/arrowup.png";
        }
        else {
            document.getElementById('more-details').style.display = 'none';   
            document.getElementById("arrow").src = "img/arrowdown.png";         
        }
    },

    //navigate to timeline page from dealer portal
    timelinepage: function() {
        document.location.href = "timeline.html";
    },

    //get display for ymdal dealer details
    getDisplay: function(id){
        var element = document.getElementById(id);
        return element.currentSytle ? element.currentSytle['display'] : window.getComputedStyle ? 
            window.getComputedStyle(element, null).getPropertyValue('display') : null;
    },
    
    //create ajax for the ymdal dealer details from json
    get_data: function() {
         $.ajax({
            type: 'GET',
            url: this.base_url + 'sample_data.php',
            success: function (result){
                console.log("DR Details: " + result);
                     var json = JSON.parse(result);
                         document.getElementById('drno').innerHTML= document.getElementById('drno').childNodes[0].nodeValue + " " + json.dr_no;
                         document.getElementById('atpno').innerHTML = document.getElementById('atpno').childNodes[0].nodeValue + " " + json.atp_no;
                         document.getElementById('date_lbl').innerHTML = document.getElementById('date_lbl').childNodes[0].nodeValue + " " + json.dr_date;
                         document.getElementById('po_no').innerHTML = document.getElementById('po_no').childNodes[0].nodeValue + " " + json.po_no;
                         document.getElementById('outlet').innerHTML = document.getElementById('outlet').childNodes[0].nodeValue + " " + json.outlet;
                         document.getElementById('tel').innerHTML = document.getElementById('tel').childNodes[0].nodeValue + " " + json.tel;
                         document.getElementById('address').innerHTML = document.getElementById('address').childNodes[0].nodeValue + " " + json.address;   document.getElementById('email').innerHTML = document.getElementById('email').childNodes[0].nodeValue + " " + json.email;
                         document.getElementById('sdr_no').innerHTML = document.getElementById('sdr_no').childNodes[0].nodeValue + " " + json.sdr_no;
                         document.getElementById('carrier').innerHTML = document.getElementById('carrier').childNodes[0].nodeValue + " " + json.carrier;
            },
            error: function() {
                console.log("get_data(): ERROR");
            }
        });
    },

    //LOAD DR ITEMS - POPULATE TABLE
    load_dr_details: function() {
        var parent_table = document.getElementById('table-items');
        //FOR LOCAL JSON FILE ONLY - FOR PRESENTATION
         $.ajax({
            type: 'GET',
            url: this.base_url + 'dr_items.php',
            success: function (result){
                console.log("DR Items: " + result);
                var json = JSON.parse(result);
                for(obj in json) {
                    var tr = document.createElement('tr');
                    var td1 = document.createElement('td');
                    var node1 = document.createTextNode(json[obj].model_code);

                    var td2 = document.createElement('td');
                    var node2 = document.createTextNode(json[obj].model_name);
                    var td3 = document.createElement('td');
                    var node3 = document.createTextNode(json[obj].frame_no);

                    var td4 = document.createElement('td');
                    var node4 = document.createTextNode(json[obj].engine_no);

                    var td5 = document.createElement('td');
                    var node5 = document.createTextNode(json[obj].status);

                    //APPEND node to td and td to tr
                    td1.appendChild(node1);
                    tr.appendChild(td1);
                    td2.appendChild(node2);
                    tr.appendChild(td2);
                    td3.appendChild(node3);
                    tr.appendChild(td3);
                    td4.appendChild(node4);
                    tr.appendChild(td4);
                    td5.appendChild(node5);
                    tr.appendChild(td5);
                    parent_table.appendChild(tr);
                }
            },
            error: function() {
                console.log("load_dr_details(): ERROR");
            }
        });
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        //disabling back button
        document.addEventListener("backbutton", function (e) {
            window.plugins.toast.showShortBottom('Back button is disabled');
            e.preventDefault();
        }, false );
    }
};

app.initialize();