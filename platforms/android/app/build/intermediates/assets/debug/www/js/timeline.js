var app = {
	// Application Constructor
    initialize: function() {
        document.addEventListener("deviceready", this.onDeviceReady.bind(this), false);

        //button for navigation back to dealer portal
        var backportal = document.getElementById("backportalbtn");
        if(backportal)
            backportal.addEventListener("click", app.backportalpage);

        app.swipe_gesture();
    },

    //enable swipe gesture
    swipe_gesture: function() {
        $(document).on("pagecreate","#wrapper",function(){
            $(window).on("swipeleft",function(){
                var swipe = document.title;
                var duration = 0.5; // animation time in seconds
                var direction = "left"; // animation direction - left | right | top | bottom
                if (swipe == "dealerportal") {
                    setTimeout(function() {
                        // Change your html here
                        document.location.href = "timeline.html";
                    }, 50);
                    nativetransitions.flip(duration, direction);
                    //document.location.href = "timeline.html";
                } else if (swipe == "timeline") {
                    setTimeout(function() {
                        // Change your html here
                        document.location.href = "dealerportal.html";
                    }, 50);
                    nativetransitions.flip(duration, direction);
                    //document.location.href = "dealerportal.html";
                }
            });                       
        });
    },

    //navigate to dealer portal from timeline page
    backportalpage: function() {
        document.location.href = "dealerportal.html";
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        //disabling back button
        document.addEventListener("backbutton", function (e) {
            window.plugins.toast.showShortBottom('Back button is disabled');
            e.preventDefault();
        }, false );
    }
};

app.initialize();