var app = {
	initialize: function() {
        document.addEventListener("deviceready", this.onDeviceReady.bind(this), false);

        //button for navigation to dealer portal
        var otpbtn = document.getElementById("otpbtn");
        if(otpbtn)
            otpbtn.addEventListener("click", app.otp_btn);

        //get data
        this.base_url = 'http://192.168.254.129/ymdal_api/'; //online
        //this.base_url = 'http://localhost/ymdal_api/'; //localhost
    },

    //exit from
    //check otp for restrictions
    //WHEN USER CLICK OTP VERIFICATION
    otp_btn: function() {
        //document.location.href = "dealerportal.html";
        var otp_code = document.getElementById('otp').value;
        var div = document.getElementById('error-dialog');
        var p = div.getElementsByTagName("p");
        if(otp_code.length == 0){
                div.style.display = 'block';
                p[0].innerHTML = "Please enter your OTP";
        }else{
            //COMPARE INPUT TO OTP NUMBER STORED IN LOCALSTORAGE
            var storage = window.localStorage;
            var otp = storage.getItem('otp_no');   
            var dr_no = storage.getItem('drno');
            var outlet = storage.getItem('outlet_code');

            if(otp_code == otp){
                //REGISTER DEVICE
                storage.removeItem('otp_no');    
                app.register_device(dr_no, outlet);   
            }else{  
                div.style.display = 'block';
                p[0].innerHTML = "Incorrect OTP";
            }
        }
    },

    //REGISTER DEVICE AFTER OTP IS CONFIRMED/VERIFIED
    register_device: function(dr_no, outlet) {
        //GET DEVICE ID
        var div = document.getElementById('error-dialog');
        var p = div.getElementsByTagName("p");
        var device_id = app.device_id();
        $.ajax({
            type: 'POST',
            data: {
                'drno': dr_no,
                'outlet_code' : outlet,
                'device_id' : device_id
            }, 
            url: this.base_url + 'register_device.php', //LINK OF API LOCATED ON THE SERVER
            success: function(response) {
                var storage = window.localStorage;
                storage.removeItem('otp_no');   
                storage.removeItem('drno');
                storage.removeItem('outlet_code');
                document.getElementById('otp').value = "";
                alert('Registered Successfully! Please wait for approval to access your page.');
                document.location.href = "index.html";
                //returns 1 for successful operation 0 otherwise
                console.log("Registered successfully, please wait for approval before accessing the application.");
            },
            error: function() {
                div.style.display = 'block';
                p[0].innerHTML = "An error occured while processing your request, please try again later.";
                console.log("register_device(): ERROR");
            }
        });
    },

    //GET DEVICE ID
    device_id: function() {
        var storage = window.localStorage;
        storage.getItem('mobile_id');

        return storage.getItem('mobile_id');
    },

    exitportal: function() {
        document.location.href = "index.html";
    },
    
    onDeviceReady: function() {
        //disabling back button
        document.addEventListener("backbutton", function (e) {
            window.plugins.toast.showShortBottom('Back button is disabled');
            e.preventDefault();
        }, false );

        //swipe gesture
        //document.addEventListener("swipe", function() {
        //    $(window).bind('swipeleft', alert("sad"));
        //}, false);
    }
};

app.initialize();