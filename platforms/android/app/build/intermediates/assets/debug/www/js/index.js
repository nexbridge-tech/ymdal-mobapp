
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener("deviceready", this.onDeviceReady.bind(this), false);

        //button for navigation to otp page
        var register = document.getElementById("register");
        if(register)
            register.addEventListener("click", app.register);

        //get data
        this.base_url = 'http://192.168.254.129/ymdal_api/'; //online
        //this.base_url = 'http://localhost/ymdal_api'; //localhost

        app.check_registered();

        var messagebtn = document.getElementById("messagebtn");
        if(messagebtn) {
            messagebtn.addEventListener("click", app.exit);
        }
    },

    exit: function() {
        navigator.app.exitApp();
    },

    //check registered
    check_registered: function() {
        $.ajax({
            type: 'POST',
            data: {
                'device_id': app.device_id()
            }, 
            url: this.base_url + 'check_registration.php', //LINK OF API LOCATED ON THE SERVER
            success: function(response) {
                var res = JSON.parse(response);
                //IF REGISTERED -> CHECK DEVICE REGISTRATION STATUS
                if(res.status == "1"){
                    app.check_registered_status();
                }
            },
            error: function() {
                alert("check_registration(): ERROR");
            }
        });
    },

    //check registered status
    check_registered_status: function() {
        $.ajax({
            type: 'POST',
            data: {
                'device_id' : app.device_id()
            }, 
            url: this.base_url + 'check_registration_status.php', //LINK OF API LOCATED ON THE SERVER
            success: function(response) {
                var res = JSON.parse(response);
                //IF REGISTRATION STATUS = PENDING
                if(res.status == "0"){
                    document.getElementById('login-form').style.display = "none";
                    document.getElementById('message-form').style.display = "block";
                }
                //IF APPROVED 
                else{
                    document.location.href = "dealerportal.html";
                }

            },
            error: function() {
                console.log("check_registration_status(): ERROR");
                alert("An error occured, please try again later.");
            }
        });
    },

    //check dr and outlet code for restrictions
    register: function() {
        //document.location.href = "otp_page.html";
        var outlet = document.getElementById('outletcode').value;
        var dr_no = document.getElementById('drno').value;
        var div = document.getElementById('error-dialog');
        var p = div.getElementsByTagName("p");
        
        if(outlet.length == 0 && dr_no.length == 0){
            //display = block;
            div.style.display = 'block';
            p[0].innerHTML = "Please enter your OUTLET CODE and DR NO";
        }else {
            if(outlet.length == 0){
                div.style.display = 'block';
                p[0].innerHTML = "Please enter at least 6 digits of OUTLET CODE";
            }else if(dr_no.length == 0){
                div.style.display = 'block';
                p[0].innerHTML = "Please enter at least 6 digits of DR NO.";
            }else{
                //database
                app.send_otp(dr_no, outlet);
            }
        }
    },

    //SEND DRNO AND OUTLET TO RECEIVE OTP NUMBER
    send_otp: function(dr_no, outlet) {
        $.ajax({
            type: 'POST',
            data: {
               'drno': dr_no,
                'outlet_code' : outlet
            }, 
            url: this.base_url + 'send_otp.php', //LINK OF API LOCATED ON THE SERVER
            success: function(response) {
                console.log('Checking sent otp status: ' + response);
                var res = JSON.parse(response);
                var otp = res.otp.toString();
                //IF OTP LENGTH IS 6 DIGITS = SUCCESS
                if( otp.length == 6){
                    var storage = window.localStorage;
                    storage.setItem('otp_no', otp);
                    storage.setItem('drno', dr_no);
                    storage.setItem('outlet_code', outlet);
                    document.location.href = "otp_page.html";
                }
                //IF NOT DISPLAY REQUEST FAILED
                else{
                    div.style.display = 'block';
                    p[0].innerHTML = "Registration failed, try again later.";
                }

            },
            error: function() {
                console.log("send_otp(): ERROR");
                alert("An error occured, please try again later.");
            }
        });
    },

    //GET DEVICE ID
    device_id: function() {
        var storage = window.localStorage;
        storage.getItem('mobile_id');

        return storage.getItem('mobile_id');
        //return mobileiddd;
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        //disabling back button
        var modeliddd = device.uuid;
        var storage = window.localStorage;
        storage.setItem('mobile_id', modeliddd);

        document.addEventListener("backbutton", function (e) {
            window.plugins.toast.showShortBottom('Back button is disabled');
            e.preventDefault();
        }, false );
    }
};
app.initialize();